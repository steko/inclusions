# -*- coding: utf-8 -*-

import unittest

import inclusions as inc

class TestInclusions(unittest.TestCase):

    def test_simple(self):
        '''The most basic example.

        It is also one of the most frequent and generic inclusions.'''

        sample = 'b1t3'
        r = inc.decode(sample)
        self.assertEqual(inc.COLORS[r['color'][0]], 'bianco')
        self.assertEqual(inc.SIZE[r['size'][0]], 'piccolissimi')
        self.assertEqual(inc.SHAPE[r['shape'][0]], 'tondo')
        self.assertEqual(r['frequency'][0], 3)

    def test_simple_wrong(self):
        '''Check that invalid codes are caught properly.'''

        invalid = [
            'b1t',
            'b13',
            '1t3',
            'h1t3',
            'b1t33',
            ]

        with self.assertRaises(ValueError):
            for i in invalid:
                r = inc.decode(i)
