#! /usr/bin/env python3
# -*- coding: utf-8 -*-

import re

from collections import OrderedDict

COLORS = {
    'b': 'bianco',
    'n': 'nero',
    'r': 'rosso',
    't': 'traslucido',
    'i': 'grigio',
    'm': 'marrone',
    'g': 'giallo',
    'a': 'argentato',
    'd': 'dorato',
    's': 'scuro',               # actually, not a color but a specification
    'c': 'chiaro',              # ditto
    'x': 'non specificato',
    }

# The size classes can be made to correspond with established
# terminology for ceramic body description.

SIZE = {
    0: 'troppo piccoli',        # really, don't use this
    1: 'piccolissimi',
    2: 'piccoli',
    3: 'medi',
    4: 'medio-grandi',
    5: 'grandi',
    6: 'grandissimi',
    }

# Below here we need an OrdererDict to process the raw string
# correctly, but this is not the natural order of shape data. A more
# natural order would be from angular to sub-angular, etc.

SHAPE = OrderedDict([('sa', 'sub-angolare'),
                     ('st', 'sub-tondeggiante'),
                     ('a', 'angolare'),
                     ('t', 'tondo'),
                     ('l', 'allungato'),
                     ('g', 'grumoso'),
                     ('e', 'esploso'),
                     ('x', 'non specificato')])


def decode(string):
    '''Extract data from a single shortcode.

    - color is a list of values, most of the times of length 1
    - size is a list of values
    - shape is a list of values
    - frequency is a single value

    When more than one value is given for *color* or *size*, it
    should be assumed that both values coexist in a coherent
    association. For example, if an inclusion is short-coded as
    ``r23a3`` it consists of red, angular fragments of small AND
    medium size. One can find even ``r2345lsa4``, that is: red, sized
    from small to very large, of sub-angular and elongated shape,
    abundant.

    When more than one value is given for *shape*, they are instead
    combined to better describe the shape of inclusions. This is most
    frequent with the ``l`` value.
    '''

    code = re.compile('([a-z]+)([0-9]+)([a-z]+)([0-9])')
    g = code.match(string)
    if g:
        data = {}
        color, size, shape, freq = g.groups()
        data['color'] = [c for c in color]
        data['size'] = [int(s) for s in size]
        data['shape'] = []
        for k, v in SHAPE.items():
            m = re.search('^%s' % k, shape)
            if m:
                data['shape'].append(m.group())
                shape = shape[m.end():]
        data['frequency'] = [int(f) for f in freq]
        return data
    else:
        raise ValueError('This shortcode is not valid.')

def print_verbose(shortcode_dict):
    '''Print a verbose version of the shortcode, from its dict representation.'''

    shortcode_dict['color'] = ', '.join([COLORS[c] for c in shortcode_dict['color']])
    shortcode_dict['size'] = [SIZE[c] for c in shortcode_dict['size']]
    shortcode_dict['shape'] = [SHAPE[c] for c in shortcode_dict['shape']]

    print('Colore: {color}. Dimensioni: {size}. Forma: {shape}. Frequenza: {frequency}.\n'.format(**shortcode_dict))

def main():
    with open('sample.txt') as f:
        for l in f:
            cs = l.split()
            for c in cs:
                try:
                    print_verbose(decode(c))
                except ValueError as e:
                    print('Shortcode {} is not valid: {}'.format(c, e))


if __name__ == '__main__':
    main()

